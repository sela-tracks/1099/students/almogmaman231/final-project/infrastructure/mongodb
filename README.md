### Installing mongoDB on EKS

### Prerequisites:
- AWS CLI configured with necessary permissions.
- `kubectl` installed and configured to interact with Amazon EKS clusters.
- Helm installed to deploy MongoDB using charts.

## Automation:

For setting up:

```bash
setup.bash
```

For cleaning:

``` bash
clean.bash
```

## Manual:

## Steps

1. **Set up `kubectl` to work with EKS:**
   - Configure `kubectl` to connect to the specified Amazon EKS cluster in the `us-west-2` region.
     - **Setting up kubectl**
        ```bash
        aws eks --region us-west-2 update-kubeconfig --name final-project-cluster
        ```

2. **Install MongoDB on EKS:**
   - Use Helm to deploy MongoDB into a dedicated namespace `mongodb`.
     - **Create a new namespace `mongodb`:**
       ```bash
       kubectl create ns mongodb
       ```
     - **Set the current namespace to `mongodb`:**
       ```bash
       kubectl config set-context --current --namespace=mongodb
       ```
     - **Install MongoDB using Helm from Bitnami charts:**
       ```bash
       helm install mongodb oci://registry-1.docker.io/bitnamicharts/mongodb -n mongodb
       ```

3. **Find the username and password for MongoDB:**
   - **Username:** `root`
   - **Password:** (To retrieve, decode the secret stored in Kubernetes)
     ```bash
     kubectl get secret --namespace mongodb mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 -d
     ```

4. **Uninstallation steps**
    ```bash
    helm uninstall mongodb --namespace mongodb
    ```

    ```bash
    kubectl delete namespace mongodb
    ```

