aws eks --region us-west-2 update-kubeconfig --name final-project-cluster
kubectl create ns mongodb
kubectl config set-context --current --namespace=mongodb

helm install mongodb oci://registry-1.docker.io/bitnamicharts/mongodb -f values.yaml -n mongodb

sleep 10

echo "credentionals:"
echo root
kubectl get secret --namespace mongodb mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 -d